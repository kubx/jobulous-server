/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.command;

import com.kubx.icenine.core.messaging.Command;
import com.kubx.icenine.util.time.TemporalRatio;
import com.kubx.jobulous.recruiting.job.model.terms.ContractType;

/**
 *
 * @author Karl
 */
public class ChangeContractTerms extends Command {
    
    private ContractType type;
    
    private boolean renewable;
    
    private TemporalRatio workingHours;

    public ContractType getType() {
        return type;
    }

    public void setType(ContractType type) {
        this.type = type;
    }

    public boolean isRenewable() {
        return renewable;
    }

    public void setRenewable(boolean renewable) {
        this.renewable = renewable;
    }

    public TemporalRatio getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(TemporalRatio workingHours) {
        this.workingHours = workingHours;
    }
}
