/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.misc;

import java.time.temporal.TemporalUnit;
import javax.money.MonetaryAmount;

/**
 *
 * @author Karl
 */
public class Salary {
    
    private final MonetaryAmount amount;
    
    private final TemporalUnit basis;

    public Salary(MonetaryAmount amount, TemporalUnit basis) {
        this.amount = amount;
        this.basis = basis;
    }

    public MonetaryAmount getAmount() {
        return amount;
    }

    public TemporalUnit getBasis() {
        return basis;
    }
}
