/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.jobulous.recruiting.job.model;

import com.kubx.jobulous.recruiting.job.model.Job;

/**
 *
 * @author Karl
 */
public interface JobRepository {

    Job findJob(String id);

    Iterable<? extends Job> findUserJobs();
    
    Iterable<? extends Job> findAllPostedJobs();
}
