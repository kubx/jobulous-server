/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.model.terms;

import com.kubx.icenine.util.time.LocalDateTimeInterval;
import com.kubx.icenine.util.time.TemporalRatio;
import com.kubx.jobulous.recruiting.misc.Salary;
import java.time.Period;

/**
 *
 * @author Karl
 */
public interface ContractTerms {
    
    ContractType getType();
    
    boolean isRenewable();
    
    LocalDateTimeInterval getDates();
    
    boolean areDatesFlexible();
    
    TemporalRatio getWorkingHours();
    
    Salary getSalary();
    
    Period getPayFrequency();
}
