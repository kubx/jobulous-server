/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.jobulous.config;

import com.kubx.icenine.util.LocalizedString;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.core.convert.converter.Converter;

/**
 *
 * @author lessardk
 */
public class LocalizedStringWriteConverter implements Converter<LocalizedString, DBObject> {

    @Override
    public DBObject convert(LocalizedString s) {
        return new BasicDBObject(s.getTranslations());
    }
}
