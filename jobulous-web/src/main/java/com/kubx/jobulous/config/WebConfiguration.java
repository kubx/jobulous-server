/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.config;

import com.kubx.icenine.rest.config.EnableIceNineRestSupport;
import com.kubx.icenine.rest.view.registration.ViewRegistrar;
import com.kubx.icenine.rest.view.registration.ViewRegistration;
import com.kubx.jobulous.system.user.model.User;
import com.kubx.jobulous.system.user.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import static org.springframework.context.annotation.ScopedProxyMode.*;
import static org.springframework.web.context.WebApplicationContext.*;
import org.springframework.web.context.request.RequestContextListener;

/**
 *
 * @author Karl
 */
@Configuration
@EnableIceNineRestSupport
public class WebConfiguration {
    
    @Autowired
    private UserRepository userRepository;
    
    @Bean
    public ViewRegistration viewRegistration(ViewRegistrar viewRegistrar) {
        
        // FIXME!!!
        return viewRegistrar
                .registerPackage("com.kubx.jobulous.recruiting.job.rest")
                .registerPackage("com.kubx.jobulous.recruiting.employer.rest")
                .registerPackage("com.kubx.jobulous.recruiting.seeker.rest")
                .done();
    } 
    
    @Bean
    @Scope(scopeName = SCOPE_SESSION, proxyMode = TARGET_CLASS)
    public User currentUser() {
        return userRepository.getCurrentUser();
    }
    
    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }
}
