/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.rest;

import com.kubx.icenine.rest.view.support.ResourceViewSupport;
import com.kubx.jobulous.recruiting.employer.model.Employer;
import com.kubx.jobulous.recruiting.job.model.Job;
import com.kubx.jobulous.recruiting.job.model.JobDescription;
import com.kubx.jobulous.recruiting.job.model.status.JobStatus;
import org.springframework.hateoas.EntityLinks;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 *
 * @author Karl
 */
public class JobView extends ResourceViewSupport<Job> {

    @Override
    protected void addExtraLinks(EntityLinks entityLinks) {
        addLink(linkTo(methodOn(JobController.class).postJob(getAdaptee().getId())).withRel("posting"));
    }
    
    public Employer getEmployer() {
        return getAdaptee().getEmployer();                
    }
    
    public JobDescription getDescription() {
        return getAdaptee().getDescription();
    }
    
    public JobStatus getStatus() {
        return getAdaptee().getStatus();
    }
}
