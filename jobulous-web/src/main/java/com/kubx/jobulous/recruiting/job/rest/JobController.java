/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.rest;

import com.kubx.icenine.core.support.Controller;
import com.kubx.jobulous.recruiting.job.command.CreateJob;
import com.kubx.jobulous.recruiting.job.command.EditJob;
import com.kubx.jobulous.recruiting.job.command.PostJob;
import com.kubx.jobulous.recruiting.job.command.RemoveJob;
import com.kubx.jobulous.recruiting.job.command.UnpostJob;
import com.kubx.jobulous.recruiting.job.model.Job;
import com.kubx.jobulous.recruiting.job.model.JobDescription;
import com.kubx.jobulous.recruiting.job.model.JobRepository;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import static org.springframework.http.HttpStatus.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.*;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Karl
 */
@RestController()
@RequestMapping("/recruiting/jobs")
@ExposesResourceFor(Job.class)
public class JobController extends Controller {
            
    @Autowired
    private JobRepository repository;
    
    @RequestMapping(method = POST)
    @ResponseStatus(CREATED)
    public Job createJob(@RequestBody @Valid CreateJob command) {
        return send(command); 
    }
    
    @RequestMapping(path = "/{id}", method = GET)
    public Job getJob(@PathVariable("id") String id) {
        return repository.findJob(id);
    }
    
    @RequestMapping(path = "/{id}/description", method = GET)
    public JobDescription getJobDescription(@PathVariable("id") String id, @RequestParam(required = false) String language) {
        Job job = repository.findJob(id);
        return null; // TODO job.getDescription(Locale.forLanguageTag(language));
    }
    
    @RequestMapping(path = "/{id}", method = DELETE)
    public void deleteJob(@PathVariable("id") String id) {
        send(id, new RemoveJob());
    }
    
    @RequestMapping(path = "/{id}", method = PATCH)
    public Job patchJob(@PathVariable("id") String id, @RequestBody EditJob command) {
        return send(id, command);
    }
    
    @RequestMapping(path = "/{id}/posting", method = PUT)
    public Job postJob(@PathVariable("id") String id) {
        return send(id, new PostJob());
    }
    
    @RequestMapping(path = "/{id}/posting", method = DELETE)
    public Job unpostJob(@PathVariable("id") String id) {
        return send(id, new UnpostJob());
    }
    
    @RequestMapping(method = GET)
    public Iterable<? extends Job> listJobs() {
        return repository.findUserJobs();
    }
    
    @RequestMapping(path = "/postings", method = GET) 
    public Iterable<? extends Job> listJobPostings() {
        return repository.findAllPostedJobs();
    }
}
