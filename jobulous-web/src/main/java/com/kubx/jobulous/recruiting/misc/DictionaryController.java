/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.jobulous.recruiting.misc;

import com.kubx.icenine.core.dictionary.Dictionary;
import com.kubx.icenine.util.LString;
import com.kubx.icenine.util.Listable;
import com.kubx.jobulous.recruiting.misc.RecruitingTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author lessardk
 */
@RestController
@RequestMapping("/recruiting/dictionary")
public class DictionaryController {
    
    @Autowired
    private Dictionary<RecruitingTopic> dictionary;
    
    @RequestMapping("/{topic}")
    public Iterable<Listable<LString>> listValues(@PathVariable("topic") RecruitingTopic topic) throws Exception {
        return dictionary.list(topic);
    }
}
