/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.seeker.rest;

import com.kubx.icenine.core.support.Controller;
import com.kubx.jobulous.recruiting.seeker.command.RegisterJobSeeker;
import com.kubx.jobulous.recruiting.seeker.command.UnregisterJobSeeker;
import com.kubx.jobulous.recruiting.seeker.model.JobSeeker;
import com.kubx.jobulous.recruiting.seeker.model.JobSeekerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import static org.springframework.http.HttpStatus.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.*;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Karl
 */
@RestController
@RequestMapping("/recruiting/seekers")
@ExposesResourceFor(JobSeeker.class)
public class JobSeekerController extends Controller {
    
    @Autowired
    private JobSeekerRepository seekerRepository;

    @RequestMapping(method = GET)
    public Iterable<? extends JobSeeker> getAllJobSeekers() {
        return seekerRepository.findAllJobSeekers();
    }
    
    @RequestMapping(method = POST)
    @ResponseStatus(CREATED)
    public JobSeeker createJobSeeker(@RequestBody RegisterJobSeeker command) {
        return send(command);
    }
    
    @RequestMapping(path = "/{id}", method = DELETE) 
    public void deleteJobSeeker(@PathVariable("id") String id) {
        send(id, new UnregisterJobSeeker());
    }
    
    @RequestMapping(path = "/{id}", method = GET)
    public JobSeeker getJobSeeker(@PathVariable("id") String id) {
        return seekerRepository.findJobSeeker(id);
    }
        
    @RequestMapping(path = "/user", method = GET)
    public JobSeeker getJobSeekerUserProfile() {
        return seekerRepository.getUserProfile();
    }
}
