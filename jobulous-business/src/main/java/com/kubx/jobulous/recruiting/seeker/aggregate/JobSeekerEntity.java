/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.seeker.aggregate;

import com.kubx.icenine.core.support.RootEntity;
import com.kubx.jobulous.recruiting.seeker.command.RegisterJobSeeker;
import com.kubx.jobulous.recruiting.seeker.command.UnregisterJobSeeker;
import com.kubx.jobulous.recruiting.seeker.data.JobSeekerData;
import com.kubx.jobulous.recruiting.seeker.event.JobSeekerRegistered;
import com.kubx.jobulous.recruiting.seeker.event.JobSeekerUnregistered;
import com.kubx.jobulous.recruiting.seeker.model.JobSeeker;
import org.axonframework.commandhandling.annotation.CommandHandler;

/**
 *
 * @author Karl
 */
public class JobSeekerEntity extends RootEntity implements JobSeeker {

    private final JobSeekerData data;
    
    private final ResumeEntity profile;
    
    @CommandHandler
    public JobSeekerEntity(RegisterJobSeeker command, JobSeekerContext context) {
        this.data = new JobSeekerData(getId());
        data.setUserId(context.getUser().getId());
        data.setNickname(command.getNickname());
        
        profile = new ResumeEntity(data);
        
        registerEvent(new JobSeekerRegistered());
    }
    
    public JobSeekerEntity(JobSeekerData data) {
        this.data = data;
        
        profile = new ResumeEntity(data);
    }

    @Override
    public String getUserId() {
        return data.getUserId();
    }
    
    @Override
    public String getNickname() {
        return data.getNickname();
    }

    @Override
    public ResumeEntity getProfile() {
        return profile;
    }    
        
    @CommandHandler
    public void handle(UnregisterJobSeeker command) {
        markDeleted();        
        registerEvent(new JobSeekerUnregistered());
    }

    public JobSeekerData toData() {
        return data;
    }
}
