/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.jobulous.recruiting.job.data;

import com.kubx.icenine.data.mongo.Collection;
import com.kubx.jobulous.recruiting.job.data.JobData;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author Karl
 */
@Collection("postings")
public interface JobPostingDataRepository extends MongoRepository<JobData, String> {
    
}
