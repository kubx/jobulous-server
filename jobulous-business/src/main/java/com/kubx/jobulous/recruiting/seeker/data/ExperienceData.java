/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.seeker.data;

import com.kubx.jobulous.recruiting.seeker.model.resume.Experience;
import java.time.Period;
import com.kubx.icenine.util.Listable;

/**
 *
 * @author Karl
 */
public class ExperienceData implements Experience {
    
    private Period yearsOf;
    
    private Listable industry;

    @Override
    public Period yearsOf() {
        return yearsOf;
    }

    public void setYearsOf(Period yearsOf) {
        this.yearsOf = yearsOf;
    }

    @Override
    public Listable getIndustry() {
        return industry;
    }

    public void setIndustry(Listable industry) {
        this.industry = industry;
    }
}
