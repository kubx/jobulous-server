/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.model;

import com.kubx.icenine.util.time.LocalDateTimeInterval;
import com.kubx.icenine.util.time.TemporalRatio;
import com.kubx.jobulous.recruiting.job.command.ChangeContractDates;
import com.kubx.jobulous.recruiting.job.command.ChangeContractTerms;
import com.kubx.jobulous.recruiting.job.command.ChangeSalary;
import com.kubx.jobulous.recruiting.job.data.ContractTermsData;
import com.kubx.jobulous.recruiting.job.data.JobData;
import com.kubx.jobulous.recruiting.job.model.terms.ContractTerms;
import com.kubx.jobulous.recruiting.job.model.terms.ContractType;
import com.kubx.jobulous.recruiting.misc.Salary;
import java.math.BigDecimal;
import java.time.Period;
import org.axonframework.commandhandling.annotation.CommandHandler;
import org.javamoney.moneta.Money;

/**
 *
 * @author Karl
 */
public class ContractTermsEntity implements ContractTerms {

    private ContractTermsData data;

    private LocalDateTimeInterval dates;
    
    private TemporalRatio workingHours;
        
    private Salary salary;

    public ContractTermsEntity(JobData rootData) {
        data = rootData.getTerms();
        
        if (data != null) {

            if (data.getStartDate() != null && data.getEndDate() != null) {
                dates = new LocalDateTimeInterval(data.getStartDate(), data.getEndDate());
            }            
            
            if (data.getWorkingTimeAmount() != null && data.getWorkingTimeBasis() != null) {
                workingHours = new TemporalRatio(data.getWorkingTimeAmount(), data.getWorkingTimeBasis());
            }            
            
            if (data.getSalaryAmount() != null && data.getSalaryBasis() != null) {
                salary = new Salary(Money.of(data.getSalaryAmount(), "CAD"), data.getSalaryBasis());
            }

        } else {
            data = new ContractTermsData();
        }
    }
    
    @Override
    public ContractType getType() {
        return data.getContractType();
    }

    @Override
    public LocalDateTimeInterval getDates() {
        return dates;
    }

    @Override
    public boolean areDatesFlexible() {
        return data.isFlexibleDates();
    }

    @Override
    public boolean isRenewable() {
        return data.isRenewable();
    }

    @Override
    public TemporalRatio getWorkingHours() {
        return workingHours;
    }

    @Override
    public Salary getSalary() {
        return salary;
    }

    @Override
    public Period getPayFrequency() {
        return data.getPayFrequency();
    }
    
    @CommandHandler
    public void handle(ChangeContractTerms command) {
        workingHours = command.getWorkingHours();
               
        data.setContractType(command.getType());
        data.setRenewable(command.isRenewable());
        data.setWorkingAmount(workingHours.getAmount());
        data.setWorkingBasis(workingHours.getBasis());
    }

    @CommandHandler
    public void handle(ChangeContractDates command) {
        dates = command.getDates();
        
        data.setStartDate(dates.getStart());
        data.setEndDate(dates.getEnd());
        data.setFlexibleDates(command.areDatesFlexible());
    }
    
    @CommandHandler
    public void handle(ChangeSalary command) {
        salary = command.getSalary();
        
        data.setPayFrequency(command.getPayFrequency());
        data.setSalaryAmount(salary.getAmount().getNumber().numberValue(BigDecimal.class));
        data.setSalaryBasis(salary.getBasis());
    }    
}
    