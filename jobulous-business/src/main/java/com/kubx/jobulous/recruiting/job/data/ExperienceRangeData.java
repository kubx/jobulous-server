/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.data;


import java.time.Period;
import com.kubx.icenine.util.Listable;

/**
 *
 * @author Karl
 */
public class ExperienceRangeData {
    
    private Period minimumYears;
    
    private Period maximumYears;
    
    private Listable industry;

    public Period getMinimumYears() {
        return minimumYears;
    }

    public void setMinimumYears(Period minimumYears) {
        this.minimumYears = minimumYears;
    }

    public Period getMaximumYears() {
        return maximumYears;
    }

    public void setMaximumYears(Period maximumYears) {
        this.maximumYears = maximumYears;
    }

    public Listable getIndustry() {
        return industry;
    }

    public void setIndustry(Listable industry) {
        this.industry = industry;
    }
}
