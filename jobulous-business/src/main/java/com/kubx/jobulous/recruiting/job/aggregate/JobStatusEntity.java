/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.aggregate;

import com.kubx.jobulous.recruiting.job.data.JobData;
import com.kubx.jobulous.recruiting.job.data.JobStatusData;
import com.kubx.jobulous.recruiting.job.model.status.JobState;
import com.kubx.jobulous.recruiting.job.model.status.JobStatus;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author Karl
 */
class JobStatusEntity implements JobStatus {

    private List<JobStatusData> dataList;
    
    private JobStatusData data;

    private JobStatusEntity previousStatus;
        
    JobStatusEntity(JobData rootData) {
        dataList = rootData.getStatuses();

        if (CollectionUtils.isEmpty(dataList)) {
            data = pushStatusData(JobState.DRAFT);
            
        } else {
            init(dataList.iterator());
        }
    }

    private JobStatusEntity(Iterator<JobStatusData> dataIter) {
        init(dataIter);
    }
    
    private void init(Iterator<JobStatusData> dataIter) {
        data = dataIter.next();
        
        if (dataIter.hasNext()) {
            previousStatus = new JobStatusEntity(dataIter);
        }
    }

    @Override
    public JobState getState() {
        return data.getState();
    }

    @Override
    public LocalDateTime getTime() {
        return data.getTime();
    }

    @Override
    public JobStatus getPreviousStatus() {
        return previousStatus;
    }

    @Override
    public boolean isJobPosted() {
        return data.getState() == JobState.POSTED || data.getState() == JobState.MODIFIED;
    }    
    
    void update(JobState state) {
        if (dataList == null) {
            throw new IllegalArgumentException("Only the last status entity could be updated");
        }
        previousStatus = new JobStatusEntity(dataList.iterator()); 
        
        data = pushStatusData(state);
    }
    
    private JobStatusData pushStatusData(JobState state) {
        JobStatusData statusData = new JobStatusData();
        
        statusData.setState(state);
        statusData.setTime(LocalDateTime.now());
        dataList.add(0, statusData);
        
        return statusData;
    }
}
