/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.employer.aggregate;

import com.kubx.icenine.core.mapping.ConvertingMapper;
import com.kubx.icenine.core.support.DataSourcedAggregateRepository;
import com.kubx.jobulous.recruiting.employer.data.EmployerData;
import com.kubx.jobulous.recruiting.employer.data.EmployerDataRepository;
import com.kubx.jobulous.recruiting.employer.model.Employer;
import com.kubx.jobulous.recruiting.employer.model.EmployerRepository;
import com.kubx.jobulous.system.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Karl
 */
@Repository
public class EmployerRepositoryImpl extends DataSourcedAggregateRepository<EmployerEntity, EmployerData> implements EmployerRepository {
        
    @Autowired
    private EmployerDataRepository dataRepository;
    
    @Autowired
    private User user;

    @Autowired
    public EmployerRepositoryImpl(EmployerContext aggregateContext) {
        super(EmployerEntity.class, new ConvertingMapper<>(
                (d) -> new EmployerEntity(d, aggregateContext),
                EmployerEntity::toData
        ));        
    }

    @Override
    protected PagingAndSortingRepository<EmployerData, String> getDataRepository() {
        return dataRepository;
    }

    @Override
    public EmployerEntity getUserProfile() {
        return map(dataRepository.findByUserId(user.getId()));
    }

    @Override
    public Employer findEmployer(String id) {
        return findOne(id);
    }

    @Override
    public Iterable<? extends Employer> findAllEmployers() {
        return findAll();
    }
}
