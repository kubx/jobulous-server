/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.jobulous.recruiting.job.data;

import com.kubx.jobulous.recruiting.job.data.JobData;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author Karl
 */
public interface JobDataRepository extends MongoRepository<JobData, String> {
    
    List<JobData> findByAdvertiserId(String advertiserId);
}
