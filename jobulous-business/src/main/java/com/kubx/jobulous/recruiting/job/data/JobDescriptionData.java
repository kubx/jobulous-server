/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.jobulous.recruiting.job.data;

import com.kubx.icenine.util.LString;
import com.kubx.icenine.util.Listable;

/**
 *
 * @author Karl
 */
public class JobDescriptionData {
    
    private Listable position;
    
    private LString tasks;

    public Listable getPosition() {
        return position;
    }

    public void setPosition(Listable position) {
        this.position = position;
    }

    public LString getTasks() {
        return tasks;
    }

    public void setTasks(LString tasks) {
        this.tasks = tasks;
    }
}
