/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.seeker.data;

import com.kubx.icenine.util.LString;
import java.util.Collection;
import com.kubx.icenine.util.Listable;

/**
 *
 * @author Karl
 */
public class ResumeData {
    
    private Collection<ExperienceData> experiences;
    
    private Collection<ScolarityData> education;

    private Collection<Listable> certifications;

    private Collection<Listable> associationMemberships;

    private Collection<Listable> equipments;
    
    private Collection<LString> interests;
    
    private Collection<LString> skills;

    public Collection<ExperienceData> getExperiences() {
        return experiences;
    }

    public void setExperiences(Collection<ExperienceData> experiences) {
        this.experiences = experiences;
    }

    public Collection<ScolarityData> getEducation() {
        return education;
    }

    public void setEducation(Collection<ScolarityData> education) {
        this.education = education;
    }

    public Collection<Listable> getCertifications() {
        return certifications;
    }

    public void setCertifications(Collection<Listable> certifications) {
        this.certifications = certifications;
    }

    public Collection<Listable> getAssociationMemberships() {
        return associationMemberships;
    }

    public void setAssociationMemberships(Collection<Listable> associationMemberships) {
        this.associationMemberships = associationMemberships;
    }

    public Collection<Listable> getEquipments() {
        return equipments;
    }

    public void setEquipments(Collection<Listable> equipments) {
        this.equipments = equipments;
    }

    public Collection<LString> getInterests() {
        return interests;
    }

    public void setInterests(Collection<LString> interests) {
        this.interests = interests;
    }

    public Collection<LString> getSkills() {
        return skills;
    }

    public void setSkills(Collection<LString> skills) {
        this.skills = skills;
    }
}
