/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.seeker.data;

import com.kubx.jobulous.recruiting.seeker.model.resume.Scolarity;
import com.kubx.icenine.util.Listable;

/**
 *
 * @author Karl
 */
public class ScolarityData implements Scolarity {
    
    private Listable degree;
    
    private Listable domain;

    public Listable getDegree() {
        return degree;
    }

    public void setDegree(Listable degree) {
        this.degree = degree;
    }

    public Listable getDomain() {
        return domain;
    }

    public void setDomain(Listable domain) {
        this.domain = domain;
    }
}
