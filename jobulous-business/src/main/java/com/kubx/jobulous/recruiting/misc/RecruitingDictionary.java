/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.jobulous.recruiting.misc;

import com.kubx.icenine.data.mongo.Collection;
import com.kubx.jobulous.recruiting.misc.RecruitingTopic;
import com.kubx.icenine.core.dictionary.MongoDictionaryDataRepository;

/**
 *
 * @author lessardk
 */
@Collection("recruiting")
public interface RecruitingDictionary extends MongoDictionaryDataRepository<RecruitingTopic> {
    
}
