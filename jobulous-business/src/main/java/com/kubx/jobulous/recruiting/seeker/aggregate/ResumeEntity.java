/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.seeker.aggregate;

import com.kubx.icenine.util.LString;
import com.kubx.jobulous.recruiting.seeker.data.JobSeekerData;
import com.kubx.jobulous.recruiting.seeker.data.ResumeData;
import com.kubx.jobulous.recruiting.seeker.model.resume.Experience;
import com.kubx.jobulous.recruiting.seeker.model.resume.Resume;
import com.kubx.jobulous.recruiting.seeker.model.resume.Scolarity;
import java.util.Collection;
import com.kubx.icenine.util.Listable;

/**
 *
 * @author Karl
 */
public class ResumeEntity implements Resume {
    
    private ResumeData data;
    
    public ResumeEntity(JobSeekerData rootData) {
        this.data = rootData.getResume();
    }
    
    @Override
    public Collection<? extends Experience> getExperiences() {
        return data.getExperiences();
    }

    @Override
    public Collection<? extends Scolarity> getEducation() {
        return data.getEducation();
    }

    @Override
    public Collection<Listable> getCertifications() {
        return data.getCertifications();
    }

    @Override
    public Collection<Listable> getAssociationMemberships() {
        return data.getAssociationMemberships();
    }

    @Override
    public Collection<Listable> getEquipments() {
        return data.getEquipments();
    }

    @Override
    public Collection<LString> getInterests() {
        return data.getInterests();
    }

    @Override
    public Collection<LString> getSkills() {
        return data.getSkills();
    }
}
