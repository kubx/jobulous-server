/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.employer.aggregate;

import com.kubx.icenine.core.messaging.CommandDispatcher;
import com.kubx.icenine.core.support.RootEntity;
import com.kubx.jobulous.recruiting.employer.command.ChangeCompanyName;
import com.kubx.jobulous.recruiting.employer.command.EditEmployer;
import com.kubx.jobulous.recruiting.employer.command.RegisterEmployer;
import com.kubx.jobulous.recruiting.employer.command.UnregisterEmployer;
import com.kubx.jobulous.recruiting.employer.data.EmployerData;
import com.kubx.jobulous.recruiting.employer.event.CompanyNameChanged;
import com.kubx.jobulous.recruiting.employer.event.EmployerRegistered;
import com.kubx.jobulous.recruiting.employer.event.EmployerUnregistered;
import com.kubx.jobulous.recruiting.employer.model.Employer;
import org.axonframework.commandhandling.annotation.CommandHandler;

/**
 *
 * @author Karl
 */
public class EmployerEntity extends RootEntity implements Employer {

    private final EmployerContext context;
    
    private final EmployerData data;
    
    @CommandHandler
    public EmployerEntity(RegisterEmployer command, EmployerContext context) {
        this.context = context;
        
        data = new EmployerData(getIdentifier());
        data.setUserId(context.getUser().getId());
        data.setCompanyName(command.getCompanyName());
        
        registerEvent(new EmployerRegistered());
    }
    
    public EmployerEntity(EmployerData data, EmployerContext context) {
        super(data.getId());        
        this.data = data;
        this.context = context;
    }
    
    @Override
    public String getUserId() {
        return data.getUserId();
    }

    @Override
    public String getCompanyName() {
        return data.getCompanyName();
    }
    
    @CommandHandler
    public void handle(EditEmployer command, CommandDispatcher commandDispatcher) {
        commandDispatcher.dispatch(this, command);
    }
    
    @CommandHandler
    public void handle(ChangeCompanyName command) {        
        data.setCompanyName(command.getValue()); 
        
        registerEvent(new CompanyNameChanged());
    }
        
    @CommandHandler
    public void handle(UnregisterEmployer command) {        
        markDeleted();                
        
        registerEvent(new EmployerUnregistered());
    }

    public EmployerData toData() {
        return data;
    }    
}
