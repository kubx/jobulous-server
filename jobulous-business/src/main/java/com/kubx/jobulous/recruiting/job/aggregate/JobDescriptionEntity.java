/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.aggregate;

import com.kubx.icenine.util.LString;
import com.kubx.jobulous.recruiting.job.command.ChangeJobDescription;
import com.kubx.jobulous.recruiting.job.command.CreateJob;
import com.kubx.jobulous.recruiting.job.data.JobData;
import com.kubx.jobulous.recruiting.job.data.JobDescriptionData;
import com.kubx.jobulous.recruiting.job.model.JobDescription;
import org.axonframework.commandhandling.annotation.CommandHandler;
import com.kubx.icenine.util.Listable;

/**
 *
 * @author Karl
 */
class JobDescriptionEntity implements JobDescription {
    
    private JobDescriptionData data;
    
    JobDescriptionEntity(JobData rootData, CreateJob command) {
        this(rootData);
        data.setPosition(command.getPosition());
    }
    
    JobDescriptionEntity(JobData rootData) {
        data = rootData.getDescription();   
        
        if (data == null) {
            data = new JobDescriptionData();            
            rootData.setDescription(data);
        }
    }
  
    @Override
    public Listable getPosition() {
        return data.getPosition();
    }

    @Override
    public LString getTasks() {
        return data.getTasks();
    }    
    
    @CommandHandler
    public void handle(ChangeJobDescription command) {        
        data.setPosition(command.getPosition());
        data.getTasks().merge(command.getTasks());
    } 
}
