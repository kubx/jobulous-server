/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.data;

import com.kubx.jobulous.recruiting.job.model.terms.ContractType;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAmount;
import java.time.temporal.TemporalUnit;

/**
 *
 * @author Karl
 */
public class ContractTermsData {
    
    private ContractType contractType;
    
    private LocalDateTime startDate;
    
    private LocalDateTime endDate;
    
    private boolean flexibleDates;
    
    private boolean renewable;
    
    private TemporalAmount workingAmount;
    
    private ChronoUnit workingBasis;
    
    private BigDecimal salaryAmount;
    
    private TemporalUnit salaryBasis;
    
    private Period payFrequency;

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public boolean isFlexibleDates() {
        return flexibleDates;
    }

    public void setFlexibleDates(boolean flexibleDates) {
        this.flexibleDates = flexibleDates;
    }

    public boolean isRenewable() {
        return renewable;
    }

    public void setRenewable(boolean renewable) {
        this.renewable = renewable;
    }

    public TemporalAmount getWorkingTimeAmount() {
        return workingAmount;
    }

    public void setWorkingAmount(TemporalAmount workingTimeAmount) {
        this.workingAmount = workingTimeAmount;
    }

    public ChronoUnit getWorkingTimeBasis() {
        return workingBasis;
    }

    public void setWorkingBasis(ChronoUnit workingTimeBasis) {
        this.workingBasis = workingTimeBasis;
    }

    public BigDecimal getSalaryAmount() {
        return salaryAmount;
    }

    public void setSalaryAmount(BigDecimal salaryAmount) {
        this.salaryAmount = salaryAmount;
    }

    public TemporalUnit getSalaryBasis() {
        return salaryBasis;
    }

    public void setSalaryBasis(TemporalUnit salaryBasis) {
        this.salaryBasis = salaryBasis;
    }

    public Period getPayFrequency() {
        return payFrequency;
    }

    public void setPayFrequency(Period payFrequency) {
        this.payFrequency = payFrequency;
    }
}
