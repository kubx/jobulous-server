/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.aggregate;

import com.kubx.icenine.core.support.RootEntity;
import com.kubx.icenine.core.messaging.CommandDispatcher;
import com.kubx.icenine.util.Lazy;
import com.kubx.jobulous.recruiting.employer.model.Employer;
import com.kubx.jobulous.recruiting.job.command.CreateJob;
import com.kubx.jobulous.recruiting.job.command.EditJob;
import com.kubx.jobulous.recruiting.job.command.PostJob;
import com.kubx.jobulous.recruiting.job.command.RemoveJob;
import com.kubx.jobulous.recruiting.job.command.UnpostJob;
import com.kubx.jobulous.recruiting.job.data.JobData;
import com.kubx.jobulous.recruiting.job.event.JobCreated;
import com.kubx.jobulous.recruiting.job.event.JobEdited;
import com.kubx.jobulous.recruiting.job.event.JobPosted;
import com.kubx.jobulous.recruiting.job.event.JobRemoved;
import com.kubx.jobulous.recruiting.job.event.JobUnposted;
import com.kubx.jobulous.recruiting.job.model.Job;
import com.kubx.jobulous.recruiting.job.model.profile.CandidateProfile;
import com.kubx.jobulous.recruiting.job.model.profile.ComplianceLevel;
import com.kubx.jobulous.recruiting.job.model.status.JobState;
import com.kubx.jobulous.system.user.exception.UserProfileException;
import org.axonframework.commandhandling.annotation.CommandHandler;
import org.axonframework.commandhandling.annotation.CommandHandlingMember;

/**
 *
 * @author Karl
 */
public class JobEntity extends RootEntity implements Job {
            
    private final JobContext context;
    
    private final JobData data;
    
    private final Lazy<Employer> employer = new Lazy<>();
        
    @CommandHandlingMember 
    private JobDescriptionEntity description;
    
    @CommandHandlingMember
    private JobStatusEntity status;

    @CommandHandlingMember
    private ContractTermsEntity contractTerms;        
    
    @CommandHandler
    public JobEntity(CreateJob command, JobContext context) throws UserProfileException {
        this.context = context;
        
        data = new JobData(getIdentifier());         
        data.setAdvertiserId(context.getUser().getId());
        data.setEmployerId(command.getEmployerId());
        
        description = new JobDescriptionEntity(data, command);
        status = new JobStatusEntity(data);
        contractTerms = new ContractTermsEntity(data);
        
        registerEvent(new JobCreated());
    }
    
    public JobEntity(JobData data, JobContext context) {
        super(data.getId());          
        
        this.context = context;
        this.data = data;        
        
        description = new JobDescriptionEntity(data);
        status = new JobStatusEntity(data);
        contractTerms = new ContractTermsEntity(data);
    }

    @Override
    public Employer getEmployer() {
        return employer.get(() -> context.getEmployerRepository().findEmployer(data.getEmployerId()));
    }

    @Override
    public JobStatusEntity getStatus() {
        return status;
    }

    @Override
    public JobDescriptionEntity getDescription() {
        return description;
    }

    @Override
    public ContractTermsEntity getContractTerms() {
        return contractTerms;
    }

    @Override
    public CandidateProfile getCandidateProfile(ComplianceLevel complianceLevel) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
    @CommandHandler
    public Job handle(EditJob command, CommandDispatcher commandDispatcher) {        
        commandDispatcher.dispatch(this, command);
        
        if (status.getState() == JobState.POSTED) {
            status.update(JobState.MODIFIED);
        }                
        registerEvent(new JobEdited());        
        
        return this;
    }
    
    @CommandHandler
    public Job handle(PostJob command) {        
        if (status.getState() != JobState.POSTED) {            
            status.update(JobState.POSTED);  
            
            registerEvent(new JobPosted());
        }        
        return this;
    }
    
    @CommandHandler
    public Job handle(UnpostJob command) {        
        if (status.isJobPosted()) {            
            status.update(command.isClosed() ? JobState.CLOSED : JobState.REMOVED); 
            
            registerEvent(new JobUnposted());
        }        
        return this;
    }
        
    @CommandHandler
    public void handle(RemoveJob command) {        
        markDeleted();              
        
        registerEvent(new JobRemoved());
    }
    
    public JobData toData() {
        return data;
    }
}
