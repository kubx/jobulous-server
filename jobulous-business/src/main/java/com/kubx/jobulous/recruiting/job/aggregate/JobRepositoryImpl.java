/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.aggregate;

import com.kubx.icenine.core.mapping.ConvertingMapper;
import com.kubx.icenine.core.support.DataSourcedAggregateRepository;
import com.kubx.jobulous.recruiting.job.data.JobData;
import com.kubx.jobulous.recruiting.job.data.JobDataRepository;
import com.kubx.jobulous.recruiting.job.data.JobPostingDataRepository;
import com.kubx.jobulous.recruiting.job.event.JobPosted;
import com.kubx.jobulous.recruiting.job.event.JobRemoved;
import com.kubx.jobulous.recruiting.job.event.JobUnposted;
import com.kubx.jobulous.recruiting.job.model.Job;
import com.kubx.jobulous.recruiting.job.model.JobRepository;
import com.kubx.jobulous.system.user.model.User;
import org.axonframework.eventhandling.annotation.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Karl
 */
@Repository
public class JobRepositoryImpl extends DataSourcedAggregateRepository<JobEntity, JobData> implements JobRepository {
    
    @Autowired
    private JobDataRepository dataRepository;
    
    @Autowired
    private JobPostingDataRepository postingDataRepository;

    @Autowired
    private User user;
    
    @Autowired
    public JobRepositoryImpl(JobContext aggregateContext) {
        super(JobEntity.class, new ConvertingMapper<>(
                (d) -> new JobEntity(d, aggregateContext), 
                JobEntity::toData
        ));
    }

    @Override
    protected JobDataRepository getDataRepository() {
        return dataRepository;
    }

    @Override
    public Job findJob(String id) {
        return findOne(id);
    }

    @Override
    public Iterable<? extends Job> findUserJobs() {
        return map(dataRepository.findByAdvertiserId(user.getId()));
    }
    
    @Override
    public Iterable<? extends Job> findAllPostedJobs() {
        return map(postingDataRepository.findAll());
    }
    
    @EventHandler
    public void handle(JobPosted event) {
        postingDataRepository.save(((JobEntity) event.getSource()).toData());
    }
    
    @EventHandler
    public void handle(JobUnposted event) {
        postingDataRepository.delete(event.getSource().getId());
    }
    
    @EventHandler
    public void handle(JobRemoved event) {
        if (event.getSource().getStatus().isJobPosted()) {
            postingDataRepository.delete(event.getSource().getId());
        }
    }
}