/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kubx.jobulous.recruiting.job.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.kubx.icenine.util.Listable;

/**
 *
 * @author Karl
 */
@Document(collection = "jobs")
public class JobData {
    
    @Id
    private String id;

    private Listable industry;
    
    private String advertiserId;
    
    private String employerId;
    
    private List<JobStatusData> statuses = new ArrayList<>();
    
    private JobDescriptionData description;
    
    private ContractTermsData terms;
    
    private CandidateProfileData seekedProfile;
    
    private CandidateProfileData wishedProfile;

    @JsonCreator
    public JobData(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Listable getIndustry() {
        return industry;
    }

    public void setIndustry(Listable industry) {
        this.industry = industry;
    }

    public String getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(String advertiserId) {
        this.advertiserId = advertiserId;
    }

    public String getEmployerId() {
        return employerId;
    }

    public void setEmployerId(String employerId) {
        this.employerId = employerId;
    }

    public List<JobStatusData> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<JobStatusData> statuses) {
        this.statuses = statuses;
    }

    public JobDescriptionData getDescription() {
        return description;
    }

    public void setDescription(JobDescriptionData description) {
        this.description = description;
    }

    public ContractTermsData getTerms() {
        return terms;
    }

    public void setTerms(ContractTermsData terms) {
        this.terms = terms;
    }

    public CandidateProfileData getSeekedProfile() {
        return seekedProfile;
    }

    public void setSeekedProfile(CandidateProfileData seekedProfile) {
        this.seekedProfile = seekedProfile;
    }

    public CandidateProfileData getWishedProfile() {
        return wishedProfile;
    }

    public void setWishedProfile(CandidateProfileData wishedProfile) {
        this.wishedProfile = wishedProfile;
    }

}
