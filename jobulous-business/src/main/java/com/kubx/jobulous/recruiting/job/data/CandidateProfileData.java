/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.recruiting.job.data;

import java.util.Collection;
import java.util.Locale;

/**
 *
 * @author Karl
 */
public class CandidateProfileData {
    
    private Collection<ExperienceRangeData> experiences;
    
    private boolean inclusiveExperiences;
    
    private Collection<Locale> languages;
    
    private boolean inclusiveLanguages;

    // TODO other stuff
    
    public Collection<ExperienceRangeData> getExperiences() {
        return experiences;
    }

    public void setExperiences(Collection<ExperienceRangeData> experiences) {
        this.experiences = experiences;
    }

    public boolean isInclusiveExperiences() {
        return inclusiveExperiences;
    }

    public void setInclusiveExperiences(boolean inclusiveExperiences) {
        this.inclusiveExperiences = inclusiveExperiences;
    }
    
    public Collection<Locale> getLanguages() {
        return languages;
    }

    public void setLanguages(Collection<Locale> languages) {
        this.languages = languages;
    }

    public boolean isInclusiveLanguages() {
        return inclusiveLanguages;
    }

    public void setInclusiveLanguages(boolean inclusiveLanguages) {
        this.inclusiveLanguages = inclusiveLanguages;
    }
}
