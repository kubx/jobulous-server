/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.system.user.aggregate;

import com.kubx.jobulous.system.user.model.UserRole;
import java.util.Collections;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

/**
 *
 * @author Karl
 */
public class UserRecord extends org.springframework.security.core.userdetails.User {
    
    private final String id;
    
    UserRecord(String id, String username, String password) {
        super(username, password, Collections.singleton(new SimpleGrantedAuthority(UserRole.USER.name())));
        
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
