/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.system.user.aggregate;

import com.kubx.icenine.core.support.AggregateRepository;
import com.kubx.jobulous.system.user.aggregate.UserContext;
import com.kubx.jobulous.system.user.aggregate.UserEntity;
import com.kubx.jobulous.system.user.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

/**
 *
 * @author Karl
 */
@Service
public class UserRepositoryImpl extends AggregateRepository<UserEntity> implements UserRepository {
    
    @Autowired
    private UserDetailsManager userDetailsManager;
    
    @Autowired    
    private UserContext aggregateContext;

    public UserRepositoryImpl() {
        super(UserEntity.class);
    }
    
    @Override
    public UserEntity getCurrentUser() {
        //UserRecord userRecord = (UserRecord) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userRecord = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        return new UserEntity(userRecord, aggregateContext);
    }

    @Override
    public UserEntity findUser(String username) {
        //UserRecord userRecord = (UserRecord) userDetailsManager.loadUserByUsername(username);
        UserDetails userRecord = (UserDetails) userDetailsManager.loadUserByUsername(username);
        
        return new UserEntity(userRecord, aggregateContext);
    }

    @Override
    protected void doSave(UserEntity aggregate) {
        if (aggregate.isSaved()) {
            userDetailsManager.updateUser(aggregate.getRecord());  
            
        } else {
            userDetailsManager.createUser(aggregate.getRecord());
            aggregate.markAsSaved();
        }
    }

    @Override
    protected UserEntity doLoad(Object aggregateIdentifier, Long expectedVersion) {
        return findUser((String) aggregateIdentifier);
    }

    @Override
    protected void doDelete(UserEntity aggregate) {
        userDetailsManager.deleteUser(aggregate.getUsername());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return findUser(username).getRecord();
    }
}
