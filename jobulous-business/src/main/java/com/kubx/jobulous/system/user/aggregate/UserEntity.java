/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.system.user.aggregate;

import com.kubx.icenine.core.support.RootEntity;
import com.kubx.jobulous.system.user.command.CreateAccount;
import com.kubx.jobulous.system.user.event.UserCreated;
import com.kubx.jobulous.system.user.model.User;
import org.axonframework.commandhandling.annotation.CommandHandler;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Karl
 */
public class UserEntity extends RootEntity implements User {
    
    private final UserContext context;
    
    //private UserRecord userRecord;
    private UserDetails userRecord;
    
    private boolean saved;
    
    @CommandHandler
    public UserEntity(CreateAccount command, UserContext context) {
        super(command.getUsername()); // FIXME for now, we use the username as the identifier
        
        this.context = context;
        this.saved = false;
        
        userRecord = new UserRecord(getIdentifier(), command.getUsername(), command.getPassword());
        
        registerEvent(new UserCreated());
    }    

    public UserEntity(UserDetails record, UserContext context) {
        super(record.getUsername()); // FIXME for now, we use the username as the identifier
        
        this.context = context;
        this.userRecord = record;
        this.saved = true;
    }

    @Override
    public String getUsername() {
        return userRecord.getUsername();
    }
    
    public boolean isSaved() {
        return saved;
    }
    
    public void markAsSaved() {
        saved = true;
    }
    
    public UserDetails getRecord() {
        return userRecord;
    }
}
