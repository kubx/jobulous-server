/*
 * Copyright 2016 KubX.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kubx.jobulous.config;

import com.kubx.icenine.data.config.IceNineDataConfiguration;
import com.kubx.icenine.data.mongo.ExtendedMongoRepositoryFactoryBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 *
 * @author Karl
 */
@Configuration
@EnableMongoRepositories(value = "com.kubx.jobulous", repositoryFactoryBeanClass = ExtendedMongoRepositoryFactoryBean.class)
public class PersistenceConfiguration extends IceNineDataConfiguration {
        
    private static final String JOBULOUS_DATABASE_NAME = "jobulous";
    
    @Override
    protected String getDatabaseName() {
        return JOBULOUS_DATABASE_NAME;
    }
}
